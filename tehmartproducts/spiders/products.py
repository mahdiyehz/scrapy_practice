import scrapy


class ProductsSpider(scrapy.Spider):
    name = 'products'
    allowed_domains = ['www.tehmart.ir']
    start_urls = ['http://www.tehmart.ir/']

    def parse(self, response):
        source1 = response.xpath("//ul[@class='root-category nav navbar-nav']/li/a/@href")
        source2 = response.xpath("//ul[@class='root-category nav navbar-nav']/li/ul/li/a/@href")
        
        for link1 in source1:
            yield response.follow(link1.get(), callback=self.parse_categories)
        
        for link2 in source2:
            yield response.follow(link2.get(), callback=self.parse_categories)
            
        
    def parse_categories(self, response):
        products_link = response.xpath("//a[@class='product-image']/@href")
        for link in products_link:
            yield response.follow(link.get(),callback=self.product_fields)
            
    def product_fields(self, response):
        product = response.xpath("//div[@id='productInfoContainer']")
        for field in product:
            yield {
            'name': field.xpath("//span[@id='productInfoName']/text()").extract_first().strip(),
            'real_price': field.xpath("//span[@class='productOldPrice']/text()").extract_first().strip(), # the real price of product
            'special_price': field.xpath("//span[@class='productSpecialPrice']/text()").extract_first().strip(), # the price that product sales after offers
            'image_url': field.xpath("//a[@class='product-image']/img/@src").extract_first().strip(),
            'description': field.xpath("//div[@id='productDescription']/div/p/text()").extract(),
            # 'specifications': 
                
        }
        